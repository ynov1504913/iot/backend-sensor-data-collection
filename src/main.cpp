// Crédit to https://how2electronics.com/connecting-esp8266-to-amazon-aws-iot-core-using-mqtt/

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <time.h>
#include "secrets.h"
#include "DHT.h"
 
#define PinCapteurHumTem D1
#define PinCapteurMov D0
#define PinCapteurPorte D2

#define PinCapteurLum A0

#define PinLedHum D6
#define PinLedTem D5
 
float h ;
float t;
int l;
const char* m;
const char* i;
unsigned long lastMillis = 0;
unsigned long previousMillis = 0;
const long interval = 5000;

int currentMovStateGen = LOW;
int previousMovStateGen = LOW;
DHT dht(PinCapteurHumTem, DHT11);
 
#define AWS_IOT_PUBLISH_TOPIC   "esp8266/pub"
#define AWS_IOT_SUBSCRIBE_TOPIC "esp8266/sub"
 
WiFiClientSecure net;
 
BearSSL::X509List cert(cacert);
BearSSL::X509List client_crt(device_cert);
BearSSL::PrivateKey key(privkey);
 
PubSubClient client(net);
 
time_t now;
time_t nowish = 1510592825;
 
 
void NTPConnect(void)
{
  Serial.print("Setting time using SNTP");
  configTime(TIME_ZONE * 3600, 0 * 3600, "pool.ntp.org", "time.nist.gov");
  now = time(nullptr);
  while (now < nowish)
  {
    delay(500);
    Serial.print(".");
    now = time(nullptr);
  }
  Serial.println("done!");
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print("Current time: ");
  Serial.print(asctime(&timeinfo));
}
 
 
void messageReceived(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Received [");
  Serial.print(topic);
  Serial.print("]: ");
  for (unsigned int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}
 
 
void connectAWS()
{
  delay(3000);
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.println(String("Attempting to connect to SSID: ") + String(WIFI_SSID));
 
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(1000);
  }
 
  NTPConnect();
 
  net.setTrustAnchors(&cert);
  net.setClientRSACert(&client_crt, &key);
 
  client.setServer(MQTT_HOST, 8883);
  client.setCallback(messageReceived);
 
 
  Serial.println("Connecting to AWS IOT");
 
  while (!client.connect(THINGNAME))
  {
    Serial.print(".");
    delay(1000);
  }
 
  if (!client.connected()) {
    Serial.println("AWS IoT Timeout!");
    return;
  }
  // Subscribe to a topic
  client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);
 
  Serial.println("AWS IoT Connected!");
}

//Génère un nombre aléatoire entre min et max
int random(int min, int max)
{
  return min + (rand() % (int)(max - min + 1));
}
 
void publishMessage()
{
  StaticJsonDocument<200> doc;
  // doc["time"] = millis();
  doc["humidity"] = h;
  doc["temperature"] = t;
  doc["luminosity"] = l;
  doc["motions"] = m;
  doc["intrusions"] = i;
  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer); // print to client
 
  client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);
}

/**
 * @brief Récupère la température avec le capteur DTH11
*/
double getTemperature(bool printing) {
  float temperature = dht.readTemperature();
  if (printing) {
    Serial.print("Température: ");
    Serial.println(temperature);
  }
  return temperature;
}

/**
 * @brief Récupère l'humidité avec le capteur DTH11
*/
double getHumidity(bool printing) {
  float humidity = dht.readHumidity();
  if (printing) {
    Serial.print("Humidité: ");
    Serial.println(humidity);
  }
  return humidity;
}

/**
 * @brief Allume la led de la température (rouge) si la température est supérieur à 23°C
*/
void turnLedTem (double tem) {
  if(tem > 23) {
    Serial.println(tem);
    digitalWrite(PinLedTem, HIGH);
  } else {
    digitalWrite(PinLedTem, LOW);
  }
}

/**
 * @brief Allume la led de l'humidité (bleue) si l'humidité est supérieur à 60%
*/
void turnLedHum (double hum) {
  if(hum > 60) {
    Serial.println(hum);
    digitalWrite(PinLedHum, HIGH);
  } else {
    digitalWrite(PinLedHum, LOW);
  }
}

/**
 * @brief Récupère la luminosité avec le capteur de luminosité
*/
double getLuminosity(bool printing) {
  double luminosity = analogRead(PinCapteurLum);
  if(printing) {
    Serial.print("Luminosité: ");
    Serial.println(luminosity);
  }
  return luminosity;
}

/**
 * @brief Récupère l'état du capteur de mouvement. Si un mouvement est détecté, 
 * la fonction renvoie 1, sinon 0.
*/
int getMotion(bool printing, int previousMovState) {
  int currentMovState = digitalRead(PinCapteurMov);

  if (previousMovState == LOW && currentMovState == HIGH) {
    if (printing) {Serial.println("Motion detected!");}
  }
  else if (previousMovState == HIGH && currentMovState == LOW) {
    if (printing) {Serial.println("Motion stopped!");}
  }
  return currentMovState;
}

/**
 * @brief Récupère l'état du capteur de porte. Si la porte est ouverte, la fonction renvoie 1, sinon 0.
*/
int getIntrusion(bool printing) {
  int isDoorOpen = digitalRead(PinCapteurPorte);
  if (printing) {
    if(isDoorOpen == 1) {
      Serial.println("Porte ouverte");
    } else {
      Serial.println("Porte fermée");
    }
  }
  return isDoorOpen;
}
 
void setup()
{
  Serial.begin(9600);
  connectAWS();
  dht.begin();

  pinMode(PinCapteurHumTem, INPUT);
  pinMode(PinCapteurLum, INPUT);
  pinMode(PinCapteurMov, INPUT);
  pinMode(PinCapteurPorte, INPUT);

  pinMode(PinLedHum, OUTPUT);
  pinMode(PinLedTem, OUTPUT);
}
 
void loop()
{

  const char* motions[2] = {"stopped", "detected"};
  const char* intrusions[2] = {"closed", "opened"};

  h = getHumidity(false);
  t = getTemperature(false);
  l = getLuminosity(false);
  currentMovStateGen = getMotion(false, previousMovStateGen);
  m = motions[currentMovStateGen];
  i = intrusions[getIntrusion(false)];

  turnLedHum(h);
  turnLedTem(t);

  previousMovStateGen = currentMovStateGen;
 
  if (isnan(h) || isnan(t) )  // Check if any reads failed and exit early (to try again).
  {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
 
  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("% || Temperature: "));
  Serial.print(t);
  Serial.print(F("°C || Luminosity: "));
  Serial.print(l);
  Serial.print(F(" lumens || Motion: "));
  Serial.print(m);
  Serial.print(F(" || Door: "));
  Serial.println(i);
  delay(3000);
 
  now = time(nullptr);
 
  if (!client.connected())
  {
    connectAWS();
  }
  else
  {
    client.loop();
    if (millis() - lastMillis > 5000)
    {
      lastMillis = millis();
      publishMessage();
    }
  }
}